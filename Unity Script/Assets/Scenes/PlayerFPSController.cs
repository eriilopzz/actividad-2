using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFPSController : MonoBehaviour
{
    /*public GameObject MainCamera;
    public float walkSpeed = 5f;
    public float hRotationSpeed = 100f;
    public float vRotationSpeed = 80f;
    */

    private CharacterMovement characterMovement;
    private MouseLook mouseLook;

    void Start()
    {
        /*Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        */
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        GameObject.Find("Capsule").gameObject.SetActive(false);

        characterMovement = GetComponent<CharacterMovement>();
        mouseLook = GetComponent<MouseLook>();

    }
    private void Update () 
    {
        movement();
        rotation();
    }

    /* void Update()
    {
        movement();
       
           float vCamRotation = Input.GetAxis("Mouse Y") * vRotationSpeed * Time.deltaTime;
           float hPlayerRotation = Input.GetAxis("Mouse X") * hRotationSpeed * Time.deltaTime;

           transform.Rotate(0f, hPlayerRotation, 0f);
           MainCamera.transform.Rotate(-vCamRotation, 0f, 0f);
        
    }*/

    private void movement()
    {
        float hMovementInput = Input.GetAxisRaw("Horizontal");
        float vMovementInput = Input.GetAxisRaw("Vertical");

        bool jumpInput = Input.GetButtonDown("Jump");
        bool dashInput = Input.GetButton("Dash");

        /*Vector3 movementDirection = hMovement * Vector3.right + vMovement * Vector3.forward;
        transform.Translate(movementDirection * (walkSpeed * Time.deltaTime));*/

        characterMovement.moveCharacter(hMovementInput, vMovementInput, jumpInput, dashInput);

    }
    private void rotation()
    {
        float hRotationInput = Input.GetAxis("Mouse X");
        float vRotationInput = Input.GetAxis("Mouse Y");
        mouseLook.handleRotation(hRotationInput, vRotationInput);
    }
}
